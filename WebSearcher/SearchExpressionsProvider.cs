﻿using System.Collections.Generic;

namespace AsosProgrammingTest.WebSearcher
{
    /// <summary>
    /// A class to provide some values to search the web for. In this case we are just searching for numbers in a sequence
    /// </summary>
    public class SearchExpressionsProvider
    {
        private readonly int _countToProvide;

        public SearchExpressionsProvider(int countToProvide)
        {
            _countToProvide = countToProvide;
        }

        public List<string> GetSearchExpressions()
        {
            var result = new List<string>();
            for (var i = 0; i < _countToProvide; i++)
            {
                result.Add(i.ToString());
            }
            return result;
        }
    }
}
