﻿using System;
using System.Data.Services.Client;
using Bing;
using System.Linq;

namespace AsosProgrammingTest.WebSearcher
{
    public class BingSearchRunner : ISearchRunner
    {
        private readonly BingSearchContainer _bing;
        public BingSearchRunner(BingSearchContainer bing)
        {
            _bing = bing;
        }

        public void AsyncSearch(string searchExpression, Action<SearchRunnerResult> searchFinished)
        {
            var query = _bing.Web(searchExpression, null, null, null, null, null, null, null);
            AsyncCallback asyncCallback = (c) =>
            {
                try
                {
                    var q = ((DataServiceQuery<WebResult>) c.AsyncState);
                    var webResult = q.EndExecute(c);
                    var result = webResult.Select(r => new SearchResult {Url = r.Url}).ToList();
                    searchFinished(SearchRunnerResult.Success(searchExpression, result));
                }
                catch (Exception e)
                {
                    searchFinished(SearchRunnerResult.Failure(searchExpression, e));
                }
            };
            query.BeginExecute(asyncCallback, query);
        }

    }
}
