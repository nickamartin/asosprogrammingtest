﻿using System;
using System.Net;
using Bing;

namespace AsosProgrammingTest.WebSearcher
{
    public class BingSearchRunnerFactory : ISearchRunnerFactory
    {
        private readonly string _bingKey;

        public BingSearchRunnerFactory(string bingKey)
        {
            _bingKey = bingKey;
        }

        public ISearchRunner Create()
        {
            return new BingSearchRunner(new BingSearchContainer(
                new Uri("https://api.datamarket.azure.com/Bing/Search/")) { Credentials = new NetworkCredential(_bingKey, _bingKey) });
        }
    }
}
