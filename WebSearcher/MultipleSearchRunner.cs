﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AsosProgrammingTest.WebSearcher
{
    public class MultipleSearchRunner
    {
        private readonly ISearchRunnerFactory _searchRunnerFactory;
        private readonly int _maxRetryCount;

        public MultipleSearchRunner(int maxRetryCount, ISearchRunnerFactory searchRunnerFactory)
        {
            _maxRetryCount = maxRetryCount;
            _searchRunnerFactory = searchRunnerFactory;
        }

        public SearchRunnerResult[] RunSearches(List<string> searches)
        {
            var waitHandle = new AutoResetEvent(true);
            var syncLock = new object();
            var pendingSearches = new Queue<Search> (searches.Select((search, index) => new Search(index, search)));
            var completedSearches = new Dictionary<int, SearchRunnerResult>();
            
            bool finished;
            do
            {
                waitHandle.WaitOne();
                lock (syncLock)
                {
                    while (pendingSearches.Count > 0)
                    {
                        var pendingSearch = pendingSearches.Dequeue();
                        var runner = _searchRunnerFactory.Create();
                        runner.AsyncSearch(pendingSearch.SearchExpression, (result) =>
                        {
                            lock (syncLock)
                            {
                                pendingSearch.Results.Add(result);
                                if (result.WasSuccessful || pendingSearch.Results.Count >= _maxRetryCount)
                                {
                                    completedSearches[pendingSearch.Index] = result;
                                }
                                else
                                {
                                    pendingSearches.Enqueue(pendingSearch);
                                }
                                waitHandle.Set();
                            }
                        });
                    }
                    finished = completedSearches.Count == searches.Count;
                }
            } while (!finished);
            return completedSearches.OrderBy(s => s.Key).Select(s => s.Value).ToArray();
        }

        class Search
        {
            public Search(int index, string searchExpression)
            {
                Index = index;
                SearchExpression = searchExpression;
                Results = new List<SearchRunnerResult>();
            }
            public int Index { get; private set; }
            public string SearchExpression { get; private set; }
            public List<SearchRunnerResult> Results { get; private set; }
        }
    }
}
