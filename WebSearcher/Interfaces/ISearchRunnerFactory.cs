﻿namespace AsosProgrammingTest.WebSearcher
{
    public interface ISearchRunnerFactory
    {
        ISearchRunner Create();
    }
}
