﻿using System;
using System.Collections.Generic;

namespace AsosProgrammingTest.WebSearcher
{
    public class SearchResult
    {
        public string Url { get; set; }
    }

    public class SearchRunnerResult
    {
        public static SearchRunnerResult Success(string searchExpression, List<SearchResult> result)
        {
            return new SearchRunnerResult { SearchExpression = searchExpression, WasSuccessful = true, Result = result };
        }

        public static SearchRunnerResult Failure(string searchExpression, Exception e)
        {
            return new SearchRunnerResult { SearchExpression = searchExpression, WasSuccessful = false, Exception = e };
        }

        public string SearchExpression { get; private set; }
        public bool WasSuccessful { get; private set; }
        public List<SearchResult> Result { get; private set; }
        public Exception Exception { get; private set; }
    }

    public interface ISearchRunner
    {
        void AsyncSearch(string searchExpression, Action<SearchRunnerResult> callback);
    }
}
