﻿using System;

namespace AsosProgrammingTest.WebSearcher
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var config = new Config();
                var searchExpressions = new SearchExpressionsProvider(config.NumberOfSearchesToPerform).GetSearchExpressions();
                var results = new MultipleSearchRunner(config.MaxRetryCount, new BingSearchRunnerFactory(config.BingKey)).RunSearches(searchExpressions);
                Console.Write(new ResultPrinter().PrintResults(results));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
