﻿To run the application/tests you first need to uncomment and fill in the following appSetting:

<!--<add key ="bingKey" value = "[EnterBingKeyHere]"/>-->

In the WebSearcher.exe.config file or WebSearcher.Tests.dll.config file.

Summary of How application was built:

- Wrote tests/functionality for MultipleSearchRunner to handle a single web search (ISearchRunner mocked)
- Expanded above to allow multiple synchronous seaches to occur
- Wrote BingSearchRunner and BingSearchRunnerFactory.  Original test was for successful search
- Converted the above to instead run multiple tests asynchronously
- Wrote GoogleSearchRunner (this was primarily to sanity check the ISearchRunner interface I now had to make sure it was generic enough)
- Added functionality to handle failures and retry them
- Created Config class to handle program configuration
- Put everything togther in Program.cs (with a test to test the process worked end to end)
- Refactored out the code to print the results to ResultPrinter
