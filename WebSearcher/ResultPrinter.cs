﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsosProgrammingTest.WebSearcher
{
    public class ResultPrinter
    {
        public string PrintResults(IEnumerable<SearchRunnerResult> results)
        {
            var outputBuilder = new StringBuilder();
            foreach (var result in results)
            {
                outputBuilder.AppendLine(string.Format("Results for Search '{0}'.  Success = {1}", result.SearchExpression, result.WasSuccessful));
                if (result.WasSuccessful)
                {
                    outputBuilder.AppendLine("URLs:");
                    foreach (var line in result.Result)
                    {
                        outputBuilder.AppendLine(string.Format("\t{0}", line.Url));
                    }
                }
                else
                {
                    outputBuilder.AppendLine(string.Format("\tException Details: {0}", result.Exception));
                }
            }
            return outputBuilder.ToString();
        }
    }
}
