﻿using System.Configuration;

namespace AsosProgrammingTest.WebSearcher
{
    public class Config
    {
        public string BingKey
        {
            get { return GetStringValue("bingKey"); }
        }

        public int NumberOfSearchesToPerform
        {
            get { return int.Parse(GetStringValue("numberOfSearchesToPerform")); }
        }

        public int MaxRetryCount
        {
            get { return int.Parse(GetStringValue("maxRetryCount")); }
        }

        private static string GetStringValue(string key)
        {
            var result = ConfigurationManager.AppSettings[key];
            if (result == null)
            {
                throw new ConfigurationErrorsException(string.Format("Missing the appSettings {0} value", key));
            }
            return ConfigurationManager.AppSettings[key];
        }
    }
}
