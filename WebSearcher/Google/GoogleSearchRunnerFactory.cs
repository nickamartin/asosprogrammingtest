﻿namespace AsosProgrammingTest.WebSearcher
{
    public class GoogleSearchRunnerFactory : ISearchRunnerFactory
    {
        public ISearchRunner Create()
        {
            return new GoogleSearchRunner("http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&safe=active&q={0}");
        }
    }
}
