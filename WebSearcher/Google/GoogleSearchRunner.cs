﻿using System;
using System.Net;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AsosProgrammingTest.WebSearcher
{
    public class GoogleSearchRunner : ISearchRunner
    {
        private readonly string _urlTemplate;
        
        public GoogleSearchRunner(string urlTemplate)
        {
            _urlTemplate = urlTemplate;
        }

        public void AsyncSearch(string searchQuery, Action<SearchRunnerResult> searchFinished)
        {
            var searchUrl = new Uri(string.Format(_urlTemplate, searchQuery));
            var client = new WebClient();
            client.DownloadStringCompleted += (sender, e) =>
            {
                try
                {
                    var page = e.Result;
                    var o = (JObject)JsonConvert.DeserializeObject(page);

                    var urls = (from result in o["responseData"]["results"].Children()
                                select new SearchResult { Url = result.Value<string>("url") }).ToList();
                    searchFinished(SearchRunnerResult.Success(searchQuery, urls));
                }
                catch (Exception ex)
                {
                    searchFinished(SearchRunnerResult.Failure(searchQuery, ex));
                }
            };
            client.DownloadStringAsync(searchUrl, searchFinished);
        }

    }
}
