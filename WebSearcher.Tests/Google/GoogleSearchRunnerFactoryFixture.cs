﻿using NUnit.Framework;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class GoogleSearchRunnerFactoryFixture
    {
        [Test]
        public void TestCreate()
        {
            var result = new GoogleSearchRunnerFactory().Create();
            Assert.IsNotNull(result);
        }
    }
}
