﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using NUnit.Framework;
using System.Linq;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class GoogleSearchRunnerFixture
    {
        [Test]
        public void TestSearch()
        {
            var searcher = new GoogleSearchRunner("http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&safe=active&q={0}");
            List<SearchResult> result = null;
            var autoResetEvent = new AutoResetEvent(false);
            searcher.AsyncSearch("google.com", res =>
            {
                result = res.Result;
                autoResetEvent.Set();
            });

            autoResetEvent.WaitOne();
            result.ForEach(r => Debug.WriteLine(r.Url));
            Assert.IsTrue(result.First().Url.Contains("google.com"));
        }

        [Test]
        public void TestSiteDown()
        {
            //Search using the wrong URL
            var searcher = new GoogleSearchRunner("http://bing.com/ajax/services/search/web?v=1.0&rsz=large&safe=active&q={0}");
            SearchRunnerResult result = null;
            var autoResetEvent = new AutoResetEvent(false);
            searcher.AsyncSearch("google.com", res =>
            {
                result = res;
                autoResetEvent.Set();
            });

            autoResetEvent.WaitOne();
            Assert.AreEqual(false, result.WasSuccessful);
            Assert.IsNotNull(result.Exception);
        }
    }
}
