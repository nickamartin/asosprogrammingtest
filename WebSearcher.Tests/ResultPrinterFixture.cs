﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class ResultPrinterFixture
    {
        [Test]
        public void TestPrintSuccessResult()
        {
            var printedResult =
                new ResultPrinter().PrintResults(new[]
                                                     {
                                                         SearchRunnerResult.Success(
                                                         "test1",
                                                         new List<SearchResult>
                                                             {
                                                                 new SearchResult{Url = "url1"}, 
                                                                 new SearchResult{Url = "url2"}
                                                             }),
                                                        SearchRunnerResult.Failure("test2", new Exception("message"))
                                                     });
            Assert.AreEqual("Results for Search 'test1'.  Success = True\r\nURLs:\r\n\turl1\r\n\turl2\r\nResults for Search 'test2'.  Success = False\r\n\tException Details: System.Exception: message\r\n", printedResult);
        }
    }
}
