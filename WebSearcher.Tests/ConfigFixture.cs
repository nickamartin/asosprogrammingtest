﻿using NUnit.Framework;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class ConfigFixture
    {
        [Test]
        public void TestBingKey()
        {
            Assert.IsNotNull(new Config().BingKey);
        }
        [Test]
        public void TestMaxRetryCount()
        {
            Assert.IsNotNull(new Config().MaxRetryCount);
        }
        [Test]
        public void TestNumberOfSearchesToPerform()
        {
            Assert.IsNotNull(new Config().NumberOfSearchesToPerform);
        }
    }
}
