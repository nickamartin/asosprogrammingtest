﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class ProgramFixture
    {
        [Test]
        public void TestEndToEnd()
        {
            var process = new Process{StartInfo = new ProcessStartInfo
                                                    {
                                                        UseShellExecute = false,
                                                        RedirectStandardOutput = true,
                                                        FileName = "WebSearcher.exe"
                                                    },
            };
            var allOutput = new StringBuilder();
            process.OutputDataReceived += (sender, args) => allOutput.AppendLine(args.Data);

            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
            Console.WriteLine(allOutput.ToString());
            Assert.IsTrue(allOutput.ToString().Contains("Results for Search '0'.  Success = True"));
            Assert.IsFalse(allOutput.ToString().Contains("Success = False"));
        }
    }
}
