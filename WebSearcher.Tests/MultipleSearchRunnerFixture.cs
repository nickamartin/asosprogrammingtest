﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using Rhino.Mocks;
using System.Linq;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class MultipleSearchRunnerFixture
    {
        [Test]
        public void TestRunMultipleSearchesSuccessfully()
        {
            var searchRunners = new List<ISearchRunner>();
            var runnerFactory = MockRepository.GenerateStub<ISearchRunnerFactory>();
            var searchStrings = new SearchExpressionsProvider(100).GetSearchExpressions();
            var expectedResults = new List<List<SearchResult>>();
            searchStrings.ForEach(searchString =>
            {
                var expectedResult = new List<SearchResult> { new SearchResult { Url = string.Format("url for {0}", searchString) } };
                expectedResults.Add(expectedResult);
                var runner = new FakeSearchRunner(SearchRunnerResult.Success(searchString, expectedResult));
                searchRunners.Add(runner);
                runnerFactory.Expect(r => r.Create()).Return(runner).Repeat.Once();
            });

            var results = new MultipleSearchRunner(1, runnerFactory).RunSearches(searchStrings);
            Assert.IsTrue(results.All(r => r.WasSuccessful));
            Assert.AreEqual(expectedResults.ToArray(), results.Select(r => r.Result).ToArray());
        }

        [Test]
        public void TestRetryOnException()
        {
            var runnerFactory = MockRepository.GenerateStub<ISearchRunnerFactory>();
            const string searchString = "123";
            var expectedResult = new List<SearchResult> { new SearchResult { Url = string.Format("url for {0}", searchString) } };
            var runner1 = new FakeSearchRunner(SearchRunnerResult.Failure(searchString, new Exception()));
            var runner2 = new FakeSearchRunner(SearchRunnerResult.Success(searchString, expectedResult));
            runnerFactory.Expect(r => r.Create()).Return(runner1).Repeat.Once();
            runnerFactory.Expect(r => r.Create()).Return(runner2).Repeat.Once();

            var result = new MultipleSearchRunner(5, runnerFactory).RunSearches(new List<string> { searchString }).First();
            Assert.IsTrue(result.WasSuccessful);
            Assert.AreEqual(expectedResult, result.Result);
        }

        [Test]
        public void TestFailIfTooManyErrors()
        {
            var runnerFactory = MockRepository.GenerateStub<ISearchRunnerFactory>();
            const string searchString = "123";
            runnerFactory.Expect(r => r.Create()).Return(new FakeSearchRunner(SearchRunnerResult.Failure(searchString, new Exception()))).Repeat.Times(5);

            var result = new MultipleSearchRunner(5, runnerFactory).RunSearches(new List<string> { searchString }).First();
            Assert.IsFalse(result.WasSuccessful);
        }

        class FakeSearchRunner : ISearchRunner
        {
            private readonly SearchRunnerResult _result;
            public FakeSearchRunner(SearchRunnerResult result)
            {
                _result = result;
            }

            public void AsyncSearch(string searchQuery, Action<SearchRunnerResult> callback)
            {
                new Thread(() =>
                {
                    Thread.Sleep(1000);
                    var nextResult = _result;
                    callback(nextResult);
                }).Start();
            }
        }
    }
}
