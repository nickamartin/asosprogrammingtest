﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;
using Bing;
using NUnit.Framework;
using System.Linq;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class BingSearchRunnerFixture
    {
        private string _bingKey;

        [SetUp]
        public void SetUp()
        {
            _bingKey = new Config().BingKey;
        }

        [Test]
        public void TestSearch()
        {
            var searcher = new BingSearchRunner(GetBingContainer("https://api.datamarket.azure.com/Bing/Search/"));
            List<SearchResult> result = null;
            var autoResetEvent = new AutoResetEvent(false);
            searcher.AsyncSearch("google.com", res =>
            {

                result = res.Result;
                autoResetEvent.Set();
            });

            autoResetEvent.WaitOne();
            result.ForEach(r => Debug.WriteLine(r.Url));
            Assert.IsTrue(result.First().Url.Contains("google.com"));
        }

        [Test]
        public void TestSiteDown()
        {
            var searcher = new BingSearchRunner(GetBingContainer("https://api.datamarket.azure.comm/Bing/Search/"));
            SearchRunnerResult result = null;
            var autoResetEvent = new AutoResetEvent(false);
            searcher.AsyncSearch("google.com", res =>
            {

                result = res;
                autoResetEvent.Set();
            });

            autoResetEvent.WaitOne();
            Assert.AreEqual(false, result.WasSuccessful);
            Assert.IsNotNull(result.Exception);
       }

        private BingSearchContainer GetBingContainer(string url)
        {
            return new BingSearchContainer(new Uri(url)) {Credentials = new NetworkCredential(_bingKey, _bingKey)};
        }
    }
}
