﻿using NUnit.Framework;

namespace AsosProgrammingTest.WebSearcher.Tests
{
    [TestFixture]
    public class BingSearchRunnerFactoryFixture
    {
        [Test]
        public void TestCreate()
        {
            var result = new BingSearchRunnerFactory("key").Create();
            Assert.IsNotNull(result);
        }
    }
}
